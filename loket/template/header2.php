<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <link rel="icon" href="../assets/logo.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RSU MALAHAYATI BIREUEN</title>
    <link rel="stylesheet" href="../libs/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="../libs/bootstrap/bootstrap-icons.css">
    <link rel="stylesheet" href="../libs/fontawesome/fontawesome-all.css">
    <script src="../libs/jquery/jquery-3.5.1.js"></script>
    <script src="../libs/bootstrap/bootstrap.bundle.js"></script>
    <script src="../libs/momentjs/moment.min.js"></script>
    <script src="../libs/momentjs/moment-with-locales.min.js"></script>
    <style>
        .footer {
            background: #8E00AA;
            border-top-left-radius: 150px;
            border-top-right-radius: 150px;
            font-size: 20px;
            /* font-weight: bold; */
            color: white;
        }


        @media print {

            #back *,
            #content {
                display: none;
            }

            #divcetak,
            #divcetak * {
                display: block;
            }
        }

        .swal2-container.swal2-center>.swal2-popup {
            grid-column: 2;
            grid-row: 2;
            align-self: center;
            justify-self: center;
            width: 90%;
            height: 80vh;
        }

        .swiper {
            width: 100%;
            height: 81vh;
            background: transparent !important;
            padding-left: 10px;
            padding-right: 10px;
        }
    </style>
</head>


<body id="body">