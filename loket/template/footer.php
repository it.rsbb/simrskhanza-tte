<div id="footer" class="fixed-bottom px-4 d-none d-md-block">

    <div class="footer bg-color-primary px-2 py-2 mx-auto">
        <div class="d-flex">
            <div class="mx-auto">
                <span style="font-size:20px"><i class=" fa fa-globe"></i> www.rsumalahayati.com</span>
                <span style="margin-left: 50px; font-size:20px"><i class=" fab fa-instagram"></i> rsu_malahayati_bireuen</span>
                <span style="margin-left: 50px; font-size:18px"><i class="fa fa-phone-square"></i> Call Center Rawat Jalan :
                    0822 9988 8284</span>
                <span style="margin-left: 50px; font-size:18px"><i class="fa fa-phone-square"></i> Call Center Rawat Inap : 0823 5333 6100</span>
            </div>
        </div>
    </div>
</div>
</body>